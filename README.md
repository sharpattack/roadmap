```mermaid

%%{
  init: {
    'theme': 'dark',
    'themeVariables':{
      'doneTaskBkgColor':'green',
      'taskBkgColor':'#076678',
      'activeTaskBkgColor':'#d65d0e',
      'critBkgColor':'#cc241d'
      }
  }
}%%

gantt
    title Retro Planning - Sharp'Attack 2024 - 2025

    dateFormat  DD-MM-YYYY
    axisFormat  %d-%b
    tickInterval 1w

    section Positionnement
    Orb3 POC                                               :active, vis_a, 14-10-2024, 3w
    Orb3 Estimer la précision                              :        vis_b, after vis_a, 2w
    Orb3 Intégration dans OPI                              :        vis_c, after vis_b, 1w
    Lidar Estimer la précision                             :        vis_d, after vis_c, 2w
    Lidar Amélioration (Compensation Vitesse et ICP)       :        vis_e, after vis_d, 4w
    

    section SharkOS
    Surcouche DDS pour message synchrone                    :        os_a, after vis_e, 2w
    Système de comptage de point progressif                 :        os_b, after os_a, 1w
    Amélioration de la stratégie de 'Retry'                 :        os_c, after os_a, 1w
    Cleaning Code Tiboisé                                   :active, os_f, after os_e, 3w
    Amélioration génération trajectoire                     :        os_g, after os_f, 3w
    Gestion trajectoire avant/arrière                       :        os_h, after os_g, 2w
    Refacto SharkOS                                         :active, os_i, 14-10-2024, 3w

    section Simulation
    Intégration SharkOS dans Webots                         :        sim_a, after os_i, 2w

    section Communication
    POC alternative CanOpen                                 :        com_a, 14-10-2024, 4w
    Intégration SharkOS de l'alternative                    :        com_b, after com_a, 3w

    section Pathfinding
    Pathfinding Python depuis OPI                           :        path_a, after os_a, 1w

    section Mécanique
    Conception des roues customs                            :active, mec_a, 14-10-2024, 4w
    Préhenseur conserves                                    :active, mec_b, 14-10-2024, 4w
    Préhenseur planches                                     :active, mec_c, 14-10-2024, 4w
    Base robot                                              :        mec_d, after mec_b, 5w

    section Électronique
    Gestion "Soft Start"                                    :        elec_a, 14-10-2024, 3w
    Conception carte puissance V2                           :        elec_b, after elec_a, 4w


    section Jalons
    Coupe Inter-UT                                          :milestone, 15-01-2025, 0d
    Coupe de France                                         :milestone, 30-05-2025, 0d
    Départ Bro en Corse Chinoise                            :milestone, 15-02-2025, 0d

``` 
